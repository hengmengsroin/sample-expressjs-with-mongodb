const ObjectId = require('mongoose').Types.ObjectId;
const responseHelper = require('../utils/response_helper')

module.exports = {
    idValidatorInParams: (field = "id") => (req, res, next) => {
        let id = req.params[field];
        if (ObjectId.isValid(id)) {
            return next();
        } else {
            console.log("OBJECT ID IS INVALID ❌❌");
            return responseHelper(res, 0, "ObjectId in params is invalid", null, null, {
                message: "ObjectId in params is invalid"
            });
        }
    },
    idValidatorInBody: (field = "id") => (req, res, next) => {
        let id = req.body[field];
        if (ObjectId.isValid(id)) {
            return next();
        } else {
            return responseHelper(res, 0, "ObjectId in body is invalid", null, null, {
                message: "ObjectId in body is invalid"
            });
        }
    },
}