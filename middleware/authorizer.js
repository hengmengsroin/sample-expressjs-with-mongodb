let jwt = require('jsonwebtoken')
let config = require('../config');
let User = require('../models/user')
let ObjectId = require('mongoose').Types.ObjectId

const authForOnly = function (options) {
    return function (req, res, next) {
        let authorization = req.headers.authorization || '';
        let token = req.body.token || req.query.token || req.headers['x-access-token'] || authorization.split(" ")[1];
        if (token) {
            jwt.verify(token, config.jwt_private_key, async function (err, decoded) {
                if (err) {
                    if (err.name == 'TokenExpiredError') {
                        return res.status(200).json({
                            code: 0,
                            message: 'The token is expired'
                        })
                    } else {
                        return res.status(200).json({
                            code: 0,
                            message: err.message,
                            error: err
                        })
                    }
                } else {
                    let userId = decoded._id;
                    let user = await User.findOne({
                        _id: ObjectId(userId),
                        status: 'active'
                    })
                        .select('first_name last_name email role')
                        .populate('role', 'name')
                    if (user != null) {
                        if (options.length > 0) {
                            // console.log(options)
                            let roleName = user.role.name;
                            console.log('roleName:', roleName);
                            if (options[0] != 'all' && options.indexOf(roleName) == -1) {
                                return res.status(200).json({
                                    code: 0,
                                    message: 'Permission denied',
                                    data: null,
                                })
                            }
                            // console.log("Requesting User:", user);
                            user.role = user.role;
                            req.user = user;
                            return next()
                        } else {
                            return next()
                        }
                    } else {
                        return res.status(200).json({
                            code: 0,
                            message: 'Permission denied. User not existed.',
                            data: null,
                        })
                    }
                }
            })
        } else {
            return res.status(200).json({
                code: 0,
                message: "The token is required"
            })
        }
    }
}

module.exports = authForOnly;