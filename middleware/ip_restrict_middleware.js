const Helper = require('../utils/helper');
const constant = require('../utils/constant');
module.exports = () => {
    return async function (req, res, next) {
        let requested_ip = getClientIp(req);
        console.log('-> CLIENT API: ', requested_ip);

        let allowed_ips = await Helper.getConfigFromDB(constant.allowed_payway_ips);

        // console.log(allowed_ips);
        if (allowed_ips == null) {
            next();
        } else {
            if (allowed_ips.length > 0) {
                let is_allowed = allowed_ips.some(ip => requested_ip == ip)
                if (is_allowed) {
                    next();
                } else {
                    res.status(401).json({
                        code: 0,
                        message: "unauthorized"
                    })
                }
            } else {
                next();
            }
        }
    }
}

function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;
    }
    return ipAddress;
};