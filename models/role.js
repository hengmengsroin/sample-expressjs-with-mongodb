const mongoose = require('mongoose');

const roleSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        default: null
    },
    description: {
        type: String,
        trim: true,
        default: null
    },

}, {
    timestamps: true
})
module.exports = mongoose.model('roles', roleSchema);