const config = require("../config");
const model = require("../models/model");
const userController = require('../controllers/user');
const Helper = require("../utils/helper");
const authHelper = require('../utils/auth_helper');
const { google } = require("googleapis");
const ClientError = require("../utils/client_error");
const { default: Axios } = require("axios");

const authController = {

    registerPhoneNumber: async (phone_number) => {
        const identityToolkit = google.identitytoolkit({
            auth: config.GCP_API_KEY,
            version: 'v3',
        });

        const response = await identityToolkit.relyingparty.sendVerificationCode({
            requestBody: {
                phoneNumber: phone_number,
            }
        });

        if (response.data.sessionInfo) {
            let user = await userController.findAndCreate({ phone_number, session_info: response.data.sessionInfo });
            return user;
        } else throw new Error("CANNOT GET SESSION INFO FROM GOOGLE");
    },

    verifyPhoneNumber: async ({ phone_number, code }) => {
        const identityToolkit = google.identitytoolkit({
            auth: config.GCP_API_KEY,
            version: 'v3',
        });

        let user = await userController.getOne({ phone_number });
        if (!user) throw new ClientError("USER NOT FOUND WITH THIS PHONE NUMBER");

        let sessionInfo = user.session_info;

        let result = await identityToolkit.relyingparty.verifyPhoneNumber({
            requestBody: {
                code: code,
                sessionInfo: sessionInfo,
            }
        });

        if (result.data) {
            user = user.toObject();
            user.is_new_user = result.data.isNewUser;
            let tokenData = authHelper.generateToken(user._id);
            return { user, token_data: tokenData };
        } else {
            throw new Error("CANNOT VERIFY PHONE NUMBER. TRY AGAIN")
        }
    },

    loginAndRegisterWithEmail: async ({ email, session }) => {
        let foundUser = await userController.getOne({ email });
        if (foundUser) {
            let tokenData = authHelper.generateToken(foundUser._id);
            return { user: foundUser, token_data: tokenData };
        } else {
            // create account with this email
            let createdUser = await userController.create({ email, status: 'inactive', session });
            // TODO: SEND MAIL CONTAIN CODE TO USER TO VERIFY ACCOUNT.
            createdUser.OTP = 1234;
            await createdUser.save({ session });
            return { user: foundUser, token_data: null, need_verify: true };
        }
    },

    verifyEmailAndActivateAccount: async ({ first_name, last_name, password, email, user_id, code, session }) => {
        let inactiveUser = await userController.getOne({ id: user_id, status: 'active' });
        if (inactiveUser && inactiveUser.code == code) {
            inactiveUser.status = 'active';
            inactiveUser.first_name = first_name;
            inactiveUser.last_name = last_name;
            inactiveUser.password = password;
            let updatedUser = await inactiveUser.save({ session });
            let tokenData = authHelper.generateToken(updatedUser._id);
            return { user: updatedUser, token_data: tokenData };
        } else {
            throw new ClientError("FAIL TO ACTIVATE THIS ACCOUNT.")
        }
    },

    facebookAuth: async ({ profile_id, access_token, session }) => {
        let fbResponse = await Axios.get(`https://graph.facebook.com/v6.0/${profile_id}?fields=id,first_name,last_name,email&access_token=${access_token}&method=get&pretty=0&suppress_http_code=1&transport=cors`)
        let fbData = fbResponse.data;
        console.log("fb data: ", fbData);
        if (fbData.error) throw new Error(fbData.error);
        if (fbData.id === profile_id) {
            // console.log("fb data: ", fbData);
            let email = fbData.email;
            let first_name = fbData.first_name;
            let last_name = fbData.last_name;
            let profileResponse = await Axios.get(`http://graph.facebook.com/${profile_id}/picture?type=square&&width=1020`);
            let profile_url = profileResponse.data.url;

            let foundUser = await userController.getOne({ facebook_id: profile_id });
            if (foundUser) {
                // update facebook profile url;
                foundUser.facebook.profile_url = profile_url;
                let updateUser = await foundUser.save({ session: session });
                let tokenData = authHelper.generateToken(foundUser._id);
                return { user: updateUser, token_data: tokenData };
            } else {
                // create user account base on fb info
                if (email) {
                    let foundUser = await userController.getOne({ email: email });
                    if (foundUser) {
                        // update facebook info
                        foundUser.facebook.id = profile_id;
                        foundUser.facebook.profile_url = profile_url;
                        foundUser.facebook.email = email;
                        await foundUser.save({ session: session });

                    } else {
                        // create account with email and facebook info
                        let facebookInfo = {
                            id: profile_id,
                            profile_url: profile_url,
                            email: email,
                        }
                        foundUser = await userController.create({ first_name, last_name, email: email, facebook_info: facebookInfo, session: session });
                    }
                    let tokenData = authHelper.generateToken(foundUser._id);
                    return { user: foundUser, token_data: tokenData };
                } else {
                    // create account with facebook info
                    let facebookInfo = {
                        id: profile_id,
                        profile_url: profile_url,
                        email: email,
                    }
                    let foundUser = await userController.create({ first_name, last_name, facebook_info: facebookInfo, session: session });
                    let tokenData = authHelper.generateToken(foundUser._id);
                    return { user: foundUser, token_data: tokenData };
                }
            }
        } else {
            throw new ClientError(fbData.error.message);
        }
    },

    googleAuth: async ({ id_token, session }) => {
        let googleResponse = await Axios.get(`https://oauth2.googleapis.com/tokeninfo?id_token=${id_token}`)
        let googleData = googleResponse.data;
        // console.log("google data: ", googleData)
        if (googleData.error) throw new Error(googleData.error);
        let profile_id = googleData.sub;
        let profile_url = googleData.picture ? googleData.picture.split('=')[0] + "=s1020-c" : null;
        // console.log('google profile:', profile);
        let first_name = googleData.given_name;
        let last_name = googleData.family_name;
        let verified_email = googleData.verified_email;
        let email = googleData.email;

        let foundUser = await userController.getOne({ google_id: profile_id });
        if (foundUser) {
            // update profile
            foundUser.google.profile_url = profile_url;
            let updatedUser = await foundUser.save({ session });
            let tokenData = authHelper.generateToken(foundUser._id);
            return { user: updatedUser, token_data: tokenData };
        } else {
            // create account with google info
            if (email) {
                let foundUser = await userController.getOne({ email });
                if (foundUser) {
                    foundUser.google.id = profile_id;
                    foundUser.google.email = email;
                    foundUser.google.profile_url = profile_url;
                    let updatedUser = await foundUser.save({ session });
                    let tokenData = authHelper.generateToken(foundUser._id);
                    return { user: updatedUser, token_data: tokenData };
                } else {
                    // create account with google info
                    let googleInfo = {
                        id: profile_id,
                        email: email,
                        profile_url: profile_url,
                    }
                    let createdUser = await userController.create({ first_name, last_name, google_info: googleInfo, session });
                    let tokenData = authHelper.generateToken(createdUser._id);
                    return { user: updatedUser, token_data: tokenData };
                }
            } else {
                // create account with google info
                let googleInfo = {
                    id: profile_id,
                    email: email,
                    profile_url: profile_url,
                }
                let createdUser = await userController.create({ first_name, last_name, google_info: googleInfo, session });
                let tokenData = authHelper.generateToken(createdUser._id);
                return { user: updatedUser, token_data: tokenData };
            }
        }
    }
};

module.exports = authController;