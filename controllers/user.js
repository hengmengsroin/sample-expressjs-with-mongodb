const model = require("../models/model");
const config = require("../config");
const bcrypt = require("bcrypt");
const ClientError = require("../utils/client_error");
const Constant = require('../utils/constant');

const userController = {
    getAll: async ({
        page,
        count,
        status,
        role
    }) => {
        let pagination = {};
        let query = {};
        // Query page count
        if (count) {
            pagination.count = parseInt(count);
        }
        if (page) {
            pagination.skip = parseInt(page - 1) * pagination.count;
        } else {
            pagination.skip = 0;
        }
        if (status) query.status = status;
        if (role) query.role = role;
        let totalItems = await model.users.find(query).countDocuments();
        let docs = await model.users
            .find(query)
            .select({
                password: 0,
                __v: 0,
            })
            .populate("role", "name")
            .limit(pagination.count)
            .skip(pagination.skip);
        return {
            data: docs,
            pagination: {
                page: page ? parseInt(page) : 1,
                count: pagination.count,
                total_items: totalItems,
            },
        };
    },

    getOne: async ({ id, email, session }) => {
        let query = {};
        if (id) query._id = id;
        if (email) query.email = email;
        let user = await model.users.findOne(query).session(session)
        return user;
    },

    findAndCreate: async ({ phone_number, session_info }) => {
        let role = Constant.role_id.user;
        let user = await model.users.findOneAndUpdate({ phone_number }, { $set: { phone_number, session_info, role } }, { upsert: true, new: true });
        return user;
    },

    create: async ({
        email,
        password,
        role_id = Constant.role_id.user,
    }) => {
        if (email) {
            let isEmailExisted = await this.getOne({ email });
            if (isEmailExisted) throw new ClientError("EMAIL ALREADY EXISTED.")
        }
        let user = await model.users.create({ email, password, role: role_id }, { session: session });
        return user;
    },

    update: async (id, {
        email,
        password,
        role_id }) => {
        let updates = {};
        if (password) {
            updates.password = bcrypt.hashSync(password, config.salt_rounds);
        }
        if (role_id) updates.role = role_id;
        if (email) {
            let checkEmail = await model.users.findOne({
                email: email, _id: { $ne: id }, status: 'active'
            });
            if (checkEmail) throw new ClientError("EMAIL ALREADY EXISTED.")
            updates.email = email;
        }

        let updatedResult = await model.users.findByIdAndUpdate({
            _id: id,
        }, {
            $set: updates,
        }, {
            new: true,
        });
        return updatedResult;
    },

    deleteUser: async (id) => {
        let deletedResult = await model.users.deleteOne({
            _id: id,
        });
        return deletedResult;
    },

    changeStatus: async (id, status) => {
        let updates = {};
        if (status) updates.status = status;
        let result = await model.users.findByIdAndUpdate({
            _id: id,
        }, {
            $set: updates,
        }, {
            new: true,
        });
        return result;
    },
};

module.exports = userController;