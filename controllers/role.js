const model = require("../models/model");
const ClientError = require("../utils/client_error");

const roleController = {
    getAll: async ({
        page,
        count, status
    }) => {
        let pagination = {};
        let query = {};
        // Query page count
        if (count) {
            pagination.count = parseInt(count);
        }
        if (page) {
            pagination.skip = parseInt(page - 1) * pagination.count;
        } else {
            pagination.skip = 0;
        }
        if (status) query.status = status;
        let totalItems = await model.roles.find(query).countDocuments();
        let docs = await model.roles
            .find(query)
            .limit(pagination.count)
            .skip(pagination.skip);
        return {
            data: docs,
            pagination: {
                page: page ? parseInt(page) : 1,
                count: pagination.count,
                total_items: totalItems,
            },
        };
    },

    getOne: async ({ id, name, status = 'active', session }) => {
        let query = { status };
        if (id) query._id = id;
        if (name) query.name = name;
        let user = await model.roles.findOne(query).session(session)
        return user;
    },

    create: async ({
        name, description, session,
    }) => {

        if (name) {
            let isExisted = await this.getOne({ name });
            if (isExisted) throw new ClientError("ROLE NAME ALREADY EXISTED.")
        }

        let result = await model.roles.create({ name, description }, { session: session });
        return result;
    },

    update: async (id, { name, description, session }) => {
        let updates = {};
        if (description) updates.description = description;
        if (name) {
            let checkEmail = await model.roles.findOne({
                name: name, _id: { $ne: id }
            });
            if (checkEmail) throw new ClientError("ROLE NAME ALREADY EXISTED.")
            updates.name = name;
        }

        let updatedResult = await model.roles.findByIdAndUpdate({
            _id: id,
        }, {
            $set: updates,
        }, {
            new: true,
        });
        return updatedResult;
    },

    delete: async (id) => {
        let deletedResult = await model.roles.deleteOne({
            _id: id,
        });
        return deletedResult;
    },

    changeStatus: async (id, status) => {
        let updates = {};
        if (status) updates.status = status;
        let result = await model.roles.findByIdAndUpdate({
            _id: id,
        }, {
            $set: updates,
        }, {
            new: true,
        });
        return result;
    },
};

module.exports = roleController;