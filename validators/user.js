const Joi = require('@hapi/joi');
const userValidator = {
    create: (body) => {
        const schema = Joi.object({
            email: Joi.string().required(),
            password: Joi.string().required(),
            role: Joi.string().required(),
        });
        return schema.validateAsync(body);
    },
    update: (body) => {
        const schema = Joi.object({
            role_id: Joi.string(),
            email: Joi.string(),
            password: Joi.string(),
        });
        return schema.validateAsync(body);
    },
    checkStatus: (body) => {
        const schema = Joi.object({
            status: Joi.string().required(),
        });
        return schema.validateAsync(body);
    }
}

module.exports = userValidator;