const Joi = require('@hapi/joi');
const roleValidator = {
    create: (body) => {
        const schema = Joi.object({
            name: Joi.string().required(),
            description: Joi.string(),
        });
        return schema.validateAsync(body);
    },
    update: (body) => {
        const schema = Joi.object({
            name: Joi.string(),
            description: Joi.string(),
        });
        return schema.validateAsync(body);
    },
    checkStatus: (body) => {
        const schema = Joi.object({
            status: Joi.string().required(),
        });
        return schema.validateAsync(body);
    }
}

module.exports = roleValidator;