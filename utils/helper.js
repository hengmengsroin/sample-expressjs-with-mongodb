const fs = require('fs');
const {
    createModel
} = require('mongoose-gridfs');
const model = require("../models/model");
const Id = require("valid-objectid");
const ObjectId = require('mongoose').Types.ObjectId;
const CONFIG = require('../config');
const url = CONFIG.url + '/v1';
const thisHelper = {
    toArray: (data) => {
        if (!data) return data;
        if (Array.isArray(data)) {
            return data;
        } else {
            return [data]
        }
    },
    getFullImageUrl: (id) => {
        if (id) return url + '/files/' + id
        else return null
    },
    baseUrl: url,
    toObjectId: (id) => ObjectId(id),
    isValidObjectId: (id) => Id.isValid(id),

    decimalAdjust: (type, value, exp) => {
        // If the exp is undefined or zero...
        if (typeof exp === 'undefined' || +exp === 0) {
            return Math[type](value);
        }
        value = +value;
        exp = +exp;
        // If the value is not a number or the exp is not an integer...
        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
            return NaN;
        }
        // Shift
        value = value.toString().split('e');
        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
        // Shift back
        value = value.toString().split('e');
        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
    },
    round10: (value, exp) => thisHelper.decimalAdjust(
        "round", value, exp),

    generateQR: (type = "local") => {
        let code = Date.now().toString(36).toUpperCase() + randomize('A0', 2);
        let result = code;
        if (type == "local") {
            result = "KH" + code;
        } else {
            result = "FR" + code;
        }
        return result;
    },


    shuffleArray: (a) => {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    },


    getTxNumber: (tx_id) => {
        let substring = tx_id.substring(2);
        return Number(substring);
    },

    generateTxString: (tx_number, prefix = "SR") => {
        let tx_string = tx_number.toString();
        let withZeroLeading = tx_string.padStart(9, '0');
        return prefix + withZeroLeading;
    },
}


module.exports = thisHelper;