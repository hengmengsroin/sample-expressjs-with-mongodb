const errorHandler = (res, error) => {
    console.error(error);
    let name = error.name;
    let message = error.message;
    switch (name) {
        case "CastError":
            res.json({
                code: 0,
                message: message,
                error: error,
                data: null,
            });
            break;
        case "ValidationError":
            res.json({
                code: 0,
                message: message,
                error: {
                    name: name,
                    detail: error.details
                },
                data: null,
            });
            break;
        case "ClientError":
            res.json({
                code: 0,
                message: message,
                error: {
                    name: name,
                    detail: error.details
                },
                data: null,
            });
            break;
        default:
            res.json({
                code: -1,
                message: message,
                error: error,
                data: null,
            });
            break;
    }
};

module.exports = errorHandler;