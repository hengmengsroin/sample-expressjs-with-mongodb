const config = require('../config');
const model = require('../models/model');
var Readable = require('stream').Readable;
const fs = require('fs');
const {
    createModel
} = require('mongoose-gridfs');
const ObjectId = require('mongoose').Types.ObjectId;
const Helper = require('../utils/helper');
const fileHelper = {
    storeBase64toAttachment: (base64, filename = 'filename', contentType = "image/jpeg") => {
        const Attachments = createModel({
            modelName: "attachments",
            connection: model.mongoose.connection
        });
        const imgBuffer = Buffer.from(base64, 'base64')

        var readStream = new Readable()

        readStream.push(imgBuffer)
        readStream.push(null)
        // const readStream = fs.createReadStream(file.tempFilePath);
        const options = {
            filename: filename,
            contentType: contentType
        };
        // console.debug("read stream: ", readStream)
        return new Promise((resolve, reject) => {
            Attachments.write(options, readStream, (error, file) => {
                if (error) {
                    console.log('Failed to upload file [', options.filename, "] to attachment.")
                    console.error(error);
                    reject(error);
                }
                if (file) {
                    console.log('Upload file [', options.filename, '] successfully to attachment.')
                    resolve(file);
                }
            });
        });
    },

    storeFileToAttachment: file => {
        const Attachments = createModel({
            modelName: "attachments",
            connection: model.mongoose.connection
        });

        // let filePath = OS.tmpdir() + "/" + file.name;
        const readStream = fs.createReadStream(file.tempFilePath);
        let filename = Date.now().toString() + "." + file.mimetype;
        const options = {
            filename: filename,
            contentType: file.mimetype
        };
        // console.debug("read stream: ", readStream)
        return new Promise((resolve, reject) => {
            Attachments.write(options, readStream, (error, file) => {
                if (error) {
                    console.log('Failed to upload file [', options.filename, "] to attachment.")
                    console.error(error);
                    reject(error);
                }
                if (file) {
                    console.log('Upload file [', options.filename, '] successfully to attachment.')
                    resolve(file);
                }
            });
        });
    },

    deleteFileFromAttachment: (id) => {
        const Attachment = createModel({
            modelName: "attachments",
            connection: model.mongoose.connection
        });
        if (id && Helper.isValidObjectId(id)) {
            return new Promise((resolve, reject) => {

                Attachment.unlink(ObjectId(id), error => {
                    if (error) {
                        console.log('Failed to delete file', id, ' from attachment.')
                        resolve(false)
                    } else {
                        console.log('Deleted file', id, 'successfully from attachment.')
                        resolve(true)
                    }
                });
            });
        } else {
            console.log("FILE ID IS NULL or INVALID")
            return false;
        }
    },

    attachmentsModel: () => {
        return createModel({
            modelName: "attachments",
            connection: model.mongoose.connection
        });
    },
}
module.exports = fileHelper;