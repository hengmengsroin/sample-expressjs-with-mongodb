const { response } = require("express");

const responseHelper = (res, code = 1, message = null, data = null, pagination = null, error = null) => {
    if (pagination) {
        let total_items = pagination.total_items;
        let count = pagination.count;
        let totalPages = Math.ceil(total_items / count);
        let hasNextPage = pagination.page * count < total_items;
        let hasPrevPage = pagination.page - 1 > 0;
        let nextPage = hasNextPage ? pagination.page + 1 : null;
        let prevPage = hasPrevPage ? pagination.page - 1 : null;
        pagination.total_pages = totalPages;
        pagination.has_next_page = hasNextPage;
        pagination.next_page = nextPage;
        pagination.has_prev_page = hasPrevPage;
        pagination.prev_page = prevPage;
    }
    const response = {
        code: code,
        message: message,
        data: data,
        pagination: pagination,
        error: error
    }
    return res.status(200).json(response);
}

module.exports = responseHelper;