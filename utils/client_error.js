class ClientError extends Error {
    constructor(message) {
        super(message);
        this.name = "ClientError";
        this.message = message;
        this.details = this.message;
    }
}

module.exports = ClientError;