const model = require('../models/model');
const config = require('../config');
const JWT = require("jsonwebtoken");

const authHelper = {
    generateToken: (userId) => {
        let signedData = {}
        signedData._id = userId;

        let token = JWT.sign(signedData, config.jwt_private_key, {
            expiresIn: config.token_duration
        });

        let refresh_token = JWT.sign(signedData, config.refresh_jwt_private_key, {
            expiresIn: config.refresh_token_duration
        });

        let tokenExpiredAt = Date.now() + (config.token_duration * 1000);
        let refreshTokenExpiresAt = Date.now() + (config.refresh_token_duration * 1000);

        return {
            token,
            token_expired_date: tokenExpiredAt,
            refresh_token,
            refresh_token_expired_date: refreshTokenExpiresAt
        };
    }
}

module.exports = authHelper;