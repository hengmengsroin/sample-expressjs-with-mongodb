# Sample project of ExpressJs with MongoDB

## Running Locally


#### Prerequisites
* [Git](https://git-scm.com/downloads)
* [Node JS](https://nodejs.org/en/)
* [Mongo DB](https://www.mongodb.com) (To use the Mongo DB interface as shown in the Mastery Monday youtube video, you need to install Mongo Compass)

#### 1. Clone the repo and install dependencies
```bash
git clone 
cd sample-expressjs-with-mongodb
npm i
```

#### 2. add config.js file
Create a new file naming it "config.js" at root directory and below is sample code.

```
var config = {
    // DB CONFIG
    mongoDB: {
        mongodb_host: "",
        mongodb_user: "",
        mongodb_pwd: "",
        db_name: "",
        auth_db: ""
    },

    // JWT CONFIG
    salt_rounds: 10,
    token_duration: 259200, // 3days, Token expired duration in seconds
    jwt_private_key: '',
    refresh_token_duration: 604800, // 7days
    refresh_jwt_private_key: '',

    // GOOGLE KEY
    GCP_API_KEY: ""
};

module.exports = config
```

#### 3. Startup your MongoDB
Usually this is just: `mongod` on the command line.

#### 4. Start the server
```bash
npm start
```