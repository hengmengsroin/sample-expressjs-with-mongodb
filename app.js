const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const fileUpload = require('express-fileupload');
const preventCORSMiddleware = require('./middleware/prevent_cors');
const helmet = require('helmet');
const OS = require('os');
const app = express();
// for proxy
app.set("trust proxy", true);
// prevent CORES error
app.use(preventCORSMiddleware);
// use http header security protection
app.use(helmet());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// file middleware
app.use(
  fileUpload({
    limits: {
      fileSize: 50 * 1024 * 1024
    },
    useTempFiles: true,
    tempFileDir: OS.tmpdir()
  })
);
// app.use(logger('dev'));
app.use(express.json({
  limit: "50mb"
}));
app.use(express.urlencoded({
  extended: false,
  limit: "50mb"
}));
app.use(logger('combined'));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', require('./routes/index'));
app.use('/auth', require('./routes/auth'));
app.use('/users', require('./routes/user'));
app.use('/roles', require('./routes/role'));
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
