var express = require('express');
const userController = require('../controllers/user');
const responseHelper = require('../utils/response_helper');
const errorHandler = require('../utils/error_handler');
const userValidator = require('../validators/user');
const { idValidatorInParams } = require('../middleware/id_validator');
const authForOnly = require('../middleware/authorizer');
var router = express.Router();


router.post('/', authForOnly(['Admin']), async (req, res, next) => {
	try {
		let body = await userValidator.create(req.body);
		if (req.files && req.files.profile) {
			body.profileFile = req.files.profile;
		}
		let result = await userController.create(body);
		return responseHelper(res, 1, 'CREATE ONE USER', result);
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.get('/', authForOnly(['Admin']), async (req, res, next) => {
	try {
		let query = req.query;
		let result = await userController.getAll(query);
		return responseHelper(res, 1, 'GET ALL USERS', result);
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.get('/:id', idValidatorInParams('id'), authForOnly(['Admin']), async (req, res, next) => {
	try {
		let id = req.params.id;
		let result = await userController.getOne({ id });
		if (result) {
			return responseHelper(res, 1, 'UPDATE ONE USER', result);
		} else {
			return responseHelper(res, 0, "USER NOT FOUND", result);
		}
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.patch('/:id', idValidatorInParams('id'), authForOnly(["Admin"]), async (req, res, next) => {
	try {
		let id = req.params.id;
		let body = await userValidator.update(req.body);
		let result = await userController.update(id, body);
		if (result) {
			return responseHelper(res, 1, 'UPDATE ONE USER', result);
		} else {
			return responseHelper(res, 0, "USER NOT FOUND", result);
		}
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.put('/:id', idValidatorInParams('id'), authForOnly(["Admin"]), async (req, res, next) => {
	try {
		let id = req.params.id;
		let status = await userValidator.checkStatus(req.body);
		let result = await userController.changeStatus(id, status);
		if (result) {
			return responseHelper(res, 1, 'CHANGE STATUS OF ONE USER', result);
		} else {
			return responseHelper(res, 0, "USER NOT FOUND", result);
		}
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.delete('/:id', idValidatorInParams('id'), authForOnly(["Admin"]), async (req, res, next) => {
	try {
		let id = req.params.id;
		let result = await userController.changeStatus(id, status);
		if (result) {
			return responseHelper(res, 1, 'CHANGE STATUS OF ONE USER', result);
		} else {
			return responseHelper(res, 0, "USER NOT FOUND", result);
		}
	} catch (error) {
		return errorHandler(res, error);
	}
});

module.exports = router;
