const router = require('express').Router();
const responseHelper = require('../utils/response_helper');
const authController = require('../controllers/auth');
const errorHandler = require('../utils/error_handler');
const { mongoose } = require('../models/model');

router.post('/email', async (req, res, next) => {
	let { email } = req.body;
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		if (!email) return responseHelper(res, 0, "EMAIL REQUIRES.");
		let loginData = await authController.loginAndRegisterWithEmail({ email, session });
		if (loginData) {
			await session.commitTransaction();
			session.endSession();
			return responseHelper(res, 1, "LOGIN SUCCESS", loginData);
		} else {
			await session.abortTransaction();
			session.endSession();
			return responseHelper(res, 0, "FAIL TO AUTH WITH EMAIL.");
		}
	} catch (error) {
		await session.abortTransaction();
		session.endSession();
		return errorHandler(res, error);
	}
});

router.post('/email/verify', async (req, res, next) => {
	let { user_id, code, first_name, last_name } = req.body;
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		if (!user_id) return responseHelper(res, 0, "USER ID REQUIRES.");
		if (!code) return responseHelper(res, 0, "CODE REQUIRES.");
		let loginData = await authController.verifyEmailAndActivateAccount({ user_id, first_name, last_name, code, session });
		if (loginData) {
			await session.commitTransaction();
			session.endSession();
			return responseHelper(res, 1, "LOGIN SUCCESS", loginData);
		} else {
			await session.abortTransaction();
			session.endSession();
			return responseHelper(res, 0, "FAIL TO VERIFY THIS ACCOUNT.");
		}
	} catch (error) {
		await session.abortTransaction();
		session.endSession();
		return errorHandler(res, error);
	}
});

router.post('/phone-number', async (req, res, next) => {
	let { phone_number } = req.body;
	try {
		if (!phone_number) return responseHelper(res, 0, "PHONE NUMBER REQUIRED.");

		let user = await authController.registerPhoneNumber(phone_number);
		if (user) {
			return responseHelper(res, 1, "Please enter code to verify your phone number");
		} else {
			return responseHelper(res, 0, "Fail to register phone number.");
		}
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.post('/phone-number/verify', async (req, res, next) => {
	let { phone_number, code } = req.body;
	try {
		if (!phone_number) return responseHelper(res, 0, "PHONE NUMBER REQUIRES.");
		if (!code) return responseHelper(res, 0, "VERIFICATION CODE REQUIRES.");
		let loginData = await authController.verifyPhoneNumber({ phone_number, code });
		if (loginData) {
			return responseHelper(res, 1, "LOGIN SUCCESS", loginData);
		} else {
			return responseHelper(res, 0, "Fail to verify phone number.");
		}
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.post('/facebook', async (req, res, next) => {
	let { profile_id, access_token } = req.body;
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		if (!profile_id) return responseHelper(res, 0, "PROFILE ID REQUIRES.");
		if (!access_token) return responseHelper(res, 0, "ACCESS TOKEN REQUIRES.");
		let loginData = await authController.facebookAuth({ profile_id, access_token, session });
		if (loginData) {
			await session.commitTransaction();
			session.endSession();
			return responseHelper(res, 1, "LOGIN SUCCESS", loginData);
		} else {
			await session.abortTransaction();
			session.endSession();
			return responseHelper(res, 0, "FAIL TO AUTH WITH FACEBOOK.");
		}
	} catch (error) {
		await session.abortTransaction();
		session.endSession();
		return errorHandler(res, error);
	}
});

router.post('/google', async (req, res, next) => {
	let { id_token } = req.body;
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		if (!id_token) return responseHelper(res, 0, "ID TOKEN REQUIRES.");
		let loginData = await authController.googleAuth({ id_token, session });
		if (loginData) {
			await session.commitTransaction();
			session.endSession();
			return responseHelper(res, 1, "LOGIN SUCCESS", loginData);
		} else {
			await session.abortTransaction();
			session.endSession();
			return responseHelper(res, 0, "FAIL TO AUTH WITH GOOGLE.");
		}
	} catch (error) {
		await session.abortTransaction();
		session.endSession();
		return errorHandler(res, error);
	}
});

module.exports = router;
