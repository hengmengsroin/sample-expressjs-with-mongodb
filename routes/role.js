var express = require('express');
const roleController = require('../controllers/role');
const responseHelper = require('../utils/response_helper');
const errorHandler = require('../utils/error_handler');
const { idValidatorInParams } = require('../middleware/id_validator');
const authForOnly = require('../middleware/authorizer');
const roleValidator = require('../validators/role');
var router = express.Router();


router.post('/', authForOnly(['Admin']), async (req, res, next) => {
	try {
		let body = await roleValidator.create(req.body);
		let result = await roleController.create(body);
		return responseHelper(res, 1, 'CREATE ONE ROLE', result);
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.get('/', authForOnly(['Admin']), async (req, res, next) => {
	try {
		let query = req.query;
		let result = await roleController.getAll(query);
		return responseHelper(res, 1, 'GET ALL ROLES', result);
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.get('/:id', idValidatorInParams('id'), authForOnly(['Admin']), async (req, res, next) => {
	try {
		let id = req.params.id;
		let result = await roleController.getOne({ id });
		if (result) {
			return responseHelper(res, 1, 'UPDATE ONE ROLE', result);
		} else {
			return responseHelper(res, 0, "ROLE NOT FOUND", result);
		}
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.patch('/:id', idValidatorInParams('id'), authForOnly(["Admin"]), async (req, res, next) => {
	try {
		let id = req.params.id;
		let body = await roleValidator.update(req.body);
		let result = await roleController.update(id, body);
		if (result) {
			return responseHelper(res, 1, 'UPDATE ONE ROLE', result);
		} else {
			return responseHelper(res, 0, "ROLE NOT FOUND", result);
		}
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.put('/:id', idValidatorInParams('id'), authForOnly(["Admin"]), async (req, res, next) => {
	try {
		let id = req.params.id;
		let status = await roleValidator.checkStatus(req.body);
		let result = await roleController.changeStatus(id, status);
		if (result) {
			return responseHelper(res, 1, 'CHANGE STATUS OF ONE ROLE', result);
		} else {
			return responseHelper(res, 0, "ROLE NOT FOUND", result);
		}
	} catch (error) {
		return errorHandler(res, error);
	}
});

router.delete('/:id', idValidatorInParams('id'), authForOnly(["Admin"]), async (req, res, next) => {
	try {
		let id = req.params.id;
		let result = await roleController.changeStatus(id, status);
		if (result) {
			return responseHelper(res, 1, 'CHANGE STATUS OF ONE ROLE', result);
		} else {
			return responseHelper(res, 0, "ROLE NOT FOUND", result);
		}
	} catch (error) {
		return errorHandler(res, error);
	}
});

module.exports = router;
