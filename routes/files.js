const router = require("express").Router();
const ObjectId = require('mongoose').Types.ObjectId;
const imageThumbnail = require('image-thumbnail');
const fileHelper = require('../utils/file_helper');
const Helper = require('../utils/helper');
const idValidatorInParams = require("../middleware/id_validator")
    .idValidatorInParams;
router.get("/:id", idValidatorInParams('id'), async (req, res) => {
    let {
        id
    } = req.params;
    let {
        width,
        height,
        quality,
        percentage,
        compression
    } = req.query;
    let option = {};
    if (width) option.width = parseFloat(width);
    if (height) option.height = parseFloat(height);
    if (percentage) option.percentage = parseFloat(percentage);
    else option.percentage = 100;
    if (quality) option.jpegOptions = {
        force: true,
        quality: parseFloat(quality)
    };
    else option.jpegOptions = {
        force: true,
        quality: 100
    };
    const Attachment = fileHelper.attachmentsModel();
    Attachment.findById(ObjectId(id), (error, attachment) => {
        if (error) {
            console.error(error);
            res.status(500).json(error);
            return;
        }
        if (attachment) {
            let contentType = attachment.contentType;
            let size = attachment.chunkSizeBytes;
            // console.log("Size:", size);
            if (compression == 1) {
                let readStream = attachment.read();
                let buffers = [];
                readStream.on('data', function (d) {
                    buffers.push(d);
                });
                readStream.on('end', function () {
                    var buf = Buffer.concat(buffers);
                    imageThumbnail(buf, option).then((thumbnail) => {
                        res.contentType(contentType);
                        res.end(thumbnail);
                        return;
                    }).catch((err) => {
                        console.log('Error: while doing imageThumbnail');
                        console.error(err);
                        res.status(404).end();
                        return;
                    })
                });
            } else {
                res.contentType(contentType);
                attachment.read().pipe(res);
            }
        } else {
            console.debug('file not found');
            res.status(404).end();
            return;
        }
    });
});
router.post("/", async (req, res) => {
    let fileObject = req.files;
    try {
        let uploadedFile = await fileHelper.storeFileToAttachment(fileObject.file);
        if (uploadedFile == null) {
            res.status(200).json({
                code: 0,
                message: "File uploading is failed. Try again.",
                data: null
            })
        } else {
            res.status(200).json({
                code: 1,
                message: "File uploaded successfully.",
                data: uploadedFile
            })
        }
    } catch (error) {
        console.log(error);
        res.status(200).json({
            code: -1,
            message: "Something went wrong. Please try again.",
            error: error
        })
    }
});
router.delete("/multiple", async (req, res) => {
    let {
        delete_file_ids
    } = req.body;
    if (!delete_file_ids) res.status(200).json({
        code: 0,
        message: "delete_file_ids is required, please input it."
    });
    try {
        delete_file_ids = Helper.toArray(delete_file_ids);
        for await (item of delete_file_ids) {
            fileHelper.deleteFileFromAttachment(item);
        }
        res.status(200).json({
            code: 1,
            message: `${delete_file_ids.length} Files deleted successfully.`,
        })
    } catch (error) {
        console.log(error);
        res.status(200).json({
            code: -1,
            message: "Error in try/catch",
            error: error
        })
    }
});
router.delete("/:id", idValidatorInParams('id'), async (req, res) => {
    let {
        id
    } = req.params;
    try {
        let result = await fileHelper.deleteFileFromAttachment(id);
        if (result) {
            res.status(200).json({
                code: 1,
                message: "File deleted successfully.",
            })
        } else {
            res.status(200).json({
                code: 0,
                message: "File deleting failed. File not found",
            })
        }
    } catch (error) {
        console.log(error);
        res.status(200).json({
            code: -1,
            message: "Something went wrong. Please try again.",
            error: error
        })
    }
});
router.post("/upload_file", async (req, res) => {
    let {
        old_file
    } = req.body;
    try {
        if (old_file) {
            let result = await fileHelper.deleteFileFromAttachment(old_file);
            console.log(`Old file has been deleted successfully (${result}).`)
        }
        if (req.files) {
            let {
                profile_img
            } = req.files;
            let uploadedFile = await fileHelper.storeFileToAttachment(profile_img);
            if (uploadedFile == null) {
                res.status(200).json({
                    code: 0,
                    message: "File uploading is failed. Try again.",
                    data: null
                })
            } else {
                res.status(200).json({
                    code: 1,
                    message: "File uploaded successfully.",
                    data: uploadedFile
                })
            }
        } else {
            res.status(200).json({
                code: 0,
                message: "File is required, please upload one file",
                data: null
            })
        }
    } catch (error) {
        console.log(error)
        res.status(200).json({
            code: -1,
            message: "Error in try/catch",
            error: error.message
        })
    }
})
module.exports = router;
